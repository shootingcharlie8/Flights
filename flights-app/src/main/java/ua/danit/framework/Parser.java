package ua.danit.framework;

import static ua.danit.framework.utils.Reflections.getMethods;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import ua.danit.framework.invocations.Invocation;
import ua.danit.framework.utils.Reflections;


public class Parser {

  /**
   * getClassInvocations(Class<?> someClass).
   * This method returns maps with all invocations for the given class,
   * where value inside @Path annotation over the method represents key.
   */

  public static Map<String, Invocation> getClassInvocations(Class<?> someClass) {
    Map<String, Invocation> resources = new HashMap<>();

    for (Method method : getMethods(someClass)) {
      if (method.isAnnotationPresent(Path.class)) {
        Invocation invoc = getInvocation(someClass, method);
        resources.put(invoc.getUrl(), invoc);
      }
    }

    return resources;
  }

  /**
   * This method.
   * Parsing the incoming class relative to the http methods and returns
   * Invocation based on this data
   */

  public static Invocation getInvocation(Class<?> someClass, Method method) {
    Path path = (Path) Reflections.getAnnotation(method, Path.class);
    String uri = path.value();
    Annotation requestMethod = getParamRequestMethod(method);

    if (requestMethod != null) {
      List<QueryParam> queryParamList = getQueryParamList(method);
      return getInvocation(someClass, method, uri, requestMethod, queryParamList);
    }
    return new Invocation(someClass, method, uri);
  }

  private static Invocation getInvocation(Class<?> someClass, Method method,
                                          String uri, Annotation requestMethod,
                                          List<QueryParam> queryParamList) {
    return queryParamList.isEmpty() ? new Invocation(someClass, method, uri, requestMethod) :
        new Invocation(someClass, method, uri, requestMethod, queryParamList);
  }

  private static Annotation getParamRequestMethod(Method method) {
    Annotation requestMethod = null;
    if (method.isAnnotationPresent(GET.class)) {
      requestMethod = Reflections.getAnnotation(method, GET.class);
    } else if (method.isAnnotationPresent(POST.class)) {
      requestMethod = Reflections.getAnnotation(method, POST.class);
    }
    return requestMethod;
  }

  private static List<QueryParam> getQueryParamList(Method method) {
    List<QueryParam> queryParams = new ArrayList<>();
    Annotation[][] parameterAnnotations = method.getParameterAnnotations();

    for (Annotation[] allAnnotations : parameterAnnotations) {
      for (Annotation paramAnnotation : allAnnotations) {
        if (paramAnnotation != null) {
          queryParams.add((QueryParam) paramAnnotation);
        }
      }
    }
    return queryParams;
  }
}
